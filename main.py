import csv
import json
import chardet


def detect_encoding(file_path):
    """
    检测文件编码方式
    :param file_path: 文件路径
    :return: 文件编码方式
    """
    with open(file_path, 'rb') as file:
        result = chardet.detect(file.read())
    return result['encoding']


def convert_csv_to_alpaca(csv_filename, json_filename, csv_encoding):
    alpaca_data = []

    # 读取CSV文件
    with open(csv_filename, mode='r', encoding='gbk') as csvfile:
        reader = csv.DictReader(csvfile)  # 假设CSV文件第一行是列名

        for row in reader:  # row: dict
            # 根据你的CSV结构，这里假设'question'是指令，'answer'是输出
            alpaca_entry = {
                # row: dict
                "instruction": row.get('title', ''),  # 使用get方法避免KeyError
                "input": row.get('ask', ''),
                "output": row.get('answer', ''),
                "category": row.get('department', ''),
                "metadata": ""  # 其他附加信息，如样本来源、创建日期、难度等级等。
            }

            # 如果有额外的列需要作为元数据添加，可以这样处理：
            # alpaca_entry["metadata"] = {
            #     "additional_info": row.get('additional_column_name', '')
            # }

            alpaca_data.append(alpaca_entry)

    # 写入JSON文件
    with open(json_filename, 'w', encoding='utf-8') as jsonfile:
        json.dump(alpaca_data, jsonfile, ensure_ascii=False, indent=4)

    print(f"Conversion complete. JSON file saved as {json_filename}")


if __name__ == '__main__':
    # csv_filename = 'your_input_file.csv'  # CSV文件路径
    csv_file = r'D:\AI\Chinese-medical-dialogue-data\Data_数据\Pediatric_儿科\儿科5-14000.csv'
    json_file = 'test/children_data.json'  # 输出JSON文件路径
    csv_encod = detect_encoding(csv_file)
    print(csv_encod)
    convert_csv_to_alpaca(csv_file, json_file, csv_encod)
