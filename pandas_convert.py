import pandas as pd


def csv_to_alpaca_json(csv_file, json_file, encoding='gb18030'):
    # 读取CSV文件
    df = pd.read_csv(csv_file, encoding=encoding)

    # 确保CSV文件有四列：'category', 'instruction', 'input', 'output'
    if len(df.columns) != 4:
        raise ValueError("CSV文件应包含四列：'category', 'instruction', 'input', 'output'")

    # 合并'instruction'和'input'列作为'prompt'
    df['prompt'] = df['title'] + "\n" + df['ask']

    # 选择需要的列并重命名以匹配Alpaca格式
    alpaca_df = df[['department', 'prompt', 'answer']].rename(columns={'department': 'tag', 'answer': 'output'})

    # 构建Alpaca格式的数据
    alpaca_data = alpaca_df.to_dict(orient='records')

    # 添加额外的结构以包含类别信息，这取决于您如何想在Alpaca格式中利用'category'信息
    # 这里我直接将'category'作为'tag'放在每个样本中，具体格式可能根据您的需求调整
    for entry in alpaca_data:
        entry['tag'] = [entry.pop('tag')]  # 将'tag'转换为列表，以便符合某些Alpaca相关应用的预期

    # 创建最终的Alpaca格式字典
    final_data = {"data": alpaca_data}

    # 将数据保存为JSON文件
    with open(json_file, 'w', encoding='utf-8') as f:
        import json
        json.dump(final_data, f, ensure_ascii=False, indent=4)

    print(f"转换完成，JSON文件已保存至：{json_file}")


if __name__ == '__main__':
    # csv_filename = 'your_input_file.csv'  # CSV文件路径
    csv_file = r'D:\AI\Chinese-medical-dialogue-data\Data_数据\Pediatric_儿科\儿科5-14000.csv'
    json_file = 'test/children_data.json'  # 输出JSON文件路径
    # 使用函数
    csv_to_alpaca_json(csv_file, json_file)
